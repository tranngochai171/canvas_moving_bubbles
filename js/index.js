const canvas = document.createElement('canvas')

canvas.width = innerWidth
canvas.height = innerHeight
const c = canvas.getContext('2d')

let mouse = {
	x: undefined,
  y: undefined,
}

const randomColor = [
  '#022601',
  '#447322',
  '#91A663',
  '#D95436',
  '#BF1304',
]

const maxRadius = 50
// const minRadius = 2

function Circle(_x, _y, _dx, _dy, _r) {
  this.x = _x;
  this.y = _y;
  this.dx = _dx;
  this.dy = _dy;
  this.r = _r
  this.color = randomColor[Math.floor(Math.random() * randomColor.length)]
  this.minRadius = _r

  this.draw = () => {
    c.beginPath();
    c.arc(this.x, this.y, this.r, 0, Math.PI * 2, false)
    c.fillStyle = this.color
    c.fill()
  }
  this.update = () => {
    if (this.x + this.r > innerWidth || this.x - this.r < 0) {
      this.dx = -this.dx
    }
    if (this.y + this.r > innerHeight || this.y - this.r < 0) {
      this.dy = -this.dy
    }
    this.x += this.dx
    this.y += this.dy
    if(mouse.x - this.x < 50 && mouse.x - this.x > -50 && mouse.y - this.y < 50 && mouse.y - this.y > -50) {
    	this.r += this.r < maxRadius ? 1 : 0
    } else {
      this.r -= this.r > this.minRadius ? 1 : 0
    }
    this.draw()
  }
}


function animate() {
  requestAnimationFrame(animate)
  c.clearRect(0, 0, innerWidth, innerHeight)
  for (let i = 0; i < circleArr.length; i++) {
    circleArr[i].update()
  }
}
let circleArr = []
function init() {
  circleArr = []
  for (let i = 0; i < 800; i++) {
    const r = Math.floor(Math.random() * 10) + 2
    const x = Math.random() * (innerWidth - r * 2) + r
    const y = Math.random() * (innerHeight - r * 2) + r
    const dx = (Math.random() - 0.5) * 2
    const dy = (Math.random() - 0.5) * 2
    circleArr.push(new Circle(x, y, dx, dy, r))
  
  }
}

animate()
window.addEventListener('resize', () => {
  canvas.width = innerWidth
  canvas.height = innerHeight
  init()
})

window.addEventListener('mousemove', (e) => {
	mouse.x = e.x
  mouse.y = e.y
})
window.addEventListener('mouseout', (e) => {
	mouse.x = undefined
  mouse.y = undefined
})
init()
document.querySelector('body').appendChild(canvas)
