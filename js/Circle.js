export default function Circle(_x, _y, _dx, _dy, _r, _c, _mouse) {
  this.x = _x;
  this.y = _y;
  this.dx = _dx;
  this.dy = _dy;
  this.r = _r;
  this.context = _c;
  this.mouse = _mouse

  this.draw = () => {
    this.context.beginPath();
    this.context.arc(this.x, this.y, this.r, 0, Math.PI * 2, false);
    this.context.stroke();
  };
  this.update = () => {
    if (this.x + this.r > innerWidth || this.x - this.r < 0) {
      this.dx = -this.dx;
    }
    if (this.y + this.r > innerHeight || this.y - this.r < 0) {
      this.dy = -this.dy;
    }
    this.x += this.dx;
    this.y += this.dy;

    if(this.mouse - this.x < 50) {
      this.r +=1
    }

    this.draw();
  };
}
